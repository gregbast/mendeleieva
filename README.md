 # Mendeleieva
Bienvenue sur le backoffice de Mendeleieva
il y a le backend avec ``supabase `` en locale dans le dossier __`supabase`__ et ``Vue et Bootstrap`` pour front dans le dossier __`mendeleievaFront`__.


## Getting started
* Pour tester back en local il vous faut
- [ ] Docker [Docs](https://docs.docker.com/engine/install/)
- [ ] Supabase Cli [Docs](https://supabase.com/docs/reference/cli/installing-and-updating)
- [ ] Fermer tous les `port 543**`

Utiliser `supabase start` et `supabase stop` pour lancer et arrêter le server.

* Pour tester front en local il vous faut 
- [ ] node. **version ``16.14.2``  _*Conseiller*_**
- [ ] ``npm i`` pour installer des dependances.
- [ ] ```npm run dev``` pour lancer le server.

!!!`.env ` n'est pas ignorer.
